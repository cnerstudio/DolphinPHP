<?php

// +----------------------------------------------------------------------
// | Library for ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2018 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://library.thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github 仓库地址 ：https://github.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------

namespace util;

/**
 * CURL数据请求管理器
 * Class Http
 * @package library\tools
 */
class Http
{
    /**
     * POST数据过滤处理
     * @param array $data 需要处理的数据
     * @param boolean $build 是否编译数据
     * @return array|string
     */
    private static function buildQueryData($data, $build = true)
    {
        if (!is_array($data)) return $data;
        foreach ($data as $key => $value){
            if (is_object($value) && $value instanceof \CURLFile) {
                $build = false;
            } elseif (is_string($value) && class_exists('CURLFile', false) && stripos($value, '@') === 0) {
                if (($filename = realpath(trim($value, '@'))) && file_exists($filename)) {
                    list($build, $data[$key]) = [false, new \CURLFile($filename)];
                }
            }
        }
        return $build ? http_build_query($data) : $data;
    }
    /**
     * 使用curl获取远程数据
     * @param  string $url HTTP请求URL地址
     * @param  array $params 请求数据
     * @param  array $header 请求头
     * @return string 获取到的数据
     */
    public static function curl_response($url, $params = [], &$header = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);                //设置访问的url地址
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);        //返回结果
        // curl_setopt($ch,CURLOPT_HEADER,1);               //是否显示头部信息
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);               //设置超时
        if ($params) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');               //设置模式
            if(in_array('Content-Type: multipart/form-data',$header??[])){
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);                  //设置数据       
            }else{
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params ));    //设置数据        
                $header[]='Content-Type: application/json';                     //设置请求头
            }
        }
        if ($header) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);        //设置请求头
        }
        // curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);   //用户访问代理 User-Agent
        // curl_setopt($ch, CURLOPT_REFERER,$_SERVER['HTTP_HOST']);        //设置 referer
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);          //跟踪301
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $response = curl_exec($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);
        $response=='{"error":"Document not found"}'?$header['download_content_length']=-1:'';
        return $response;
    }
}