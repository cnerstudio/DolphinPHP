<?php
namespace app\admin\logic;

use think\facade\Cache;

class Index
{
    /**
     * 清空系统缓存
     * @author 蔡伟明 <314013107@qq.com>
     */
    public function wipeCache($wipe_cache_type)
    {
        foreach ($wipe_cache_type as $item) {
            switch ($item) {
                case 'TEMP_PATH':
                    array_map('unlink', glob(runtime_path(). 'temp/*.*'));
                    break;
                case 'LOG_PATH':
                    $dirs = (array) glob(runtime_path() . 'log/*');
                    foreach ($dirs as $dir) {
                        array_map('unlink', glob($dir . '/*.log'));
                    }
                    array_map('rmdir', $dirs);
                    break;
                case 'CACHE_PATH':
                    array_map('unlink', glob(runtime_path(). 'cache/*.*'));
                    break;
            }
        }
        Cache::clear();
    }

}
