<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2019 广东卓锐软件有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------

namespace app\admin\model;

use app\admin\model\Config as ConfigModel;

use think\facade\Db;
use think\File;
use think\Model;
use util\File as utilFile;
use util\Http as utilHttp;
use think\facade\Env;

/**
 * 附件模型
 * @package app\admin\model
 */
class Attachment extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $name = 'admin_attachment';

    // 自动写入时间戳
    protected $autoWriteTimestamp = true;

    static protected $ext_arr = ['ai', 'bin', 'csv', 'doc', 'docx', 'eml', 'gif', 'html', 'ics', 'jpeg', 'jpg', 'jsp', 'license', 'm4a', 'mov', 'mp3', 'mp4', 'mpg', 'pdf', 'png', 'ppt', 'pptx', 'psd', 'rar', 'rtf', 'svg', 'txt', 'wav', 'webp', 'wps', 'xls', 'xlsx', 'zip'];

    static public function getAttachmentExists($where = [], $newTitle = '')
    {
        // 判断附件是否已存在
        $file_exists = self::where('status', 1)->where($where)->find();
        if ($file_exists && $file_exists['path']) {
            $full_path = public_path() . $file_exists['path']; //服务器完整路径
            if (file_exists($full_path) || $file_exists['driver'] != 'local') { //验证本地附件是否存在
                if ($newTitle != '' && $newTitle != $file_exists['name']) {
                    self::where('id', $file_exists['id'])->update(['name' => $file_exists['name']=$newTitle]);
                }
                return $file_exists;
            } else {
                self::where('id', $file_exists['id'])->delete(); //清理失效附件
                return self::getAttachmentExists($where, $newTitle);
            }
        } else {
            return null;
        }
    }

    static public function getConfig($disks)
    {
        $filesystemConfig = config('filesystem');
        switch ($disks) {
            case 'aliyun': //阿里云存储
                $filesystemConfig['disks'][$disks]['accessId'] = Env::get('FILESYSTEM.aliyunKey', '');
                $filesystemConfig['disks'][$disks]['accessSecret'] = Env::get('FILESYSTEM.aliyunSecret', '');
                break;
            case 'qiniu': //七牛云(华北)
            case 'qiniu-hd': //七牛云(华东)
                $filesystemConfig['disks'][$disks]['accessKey'] = Env::get('FILESYSTEM.qiniuAccess', '');//php7
                $filesystemConfig['disks'][$disks]['secretKey'] = Env::get('FILESYSTEM.qiniuSecret', '');//php7
                $filesystemConfig['disks'][$disks]['access_key'] = Env::get('FILESYSTEM.qiniuAccess', '');//php8
                $filesystemConfig['disks'][$disks]['secret_key'] = Env::get('FILESYSTEM.qiniuSecret', '');//php8
                break;
            default:
                # code...
                break;
        }
        config($filesystemConfig, 'filesystem'); //设置配置
        return $filesystemConfig['disks'][$disks];
    }

    static public function toAttachment($file_path, $module = '', $title = '', $disks = '', $foreign_path = '')
    { //附件化
        $disks == 'local' ? $disks = 'public' : $disks;
        $whereExists = [];
        if (!$disks) {
            //未指定取配置项
            $upload_driver = ConfigModel::getConfig('upload_driver');
            $disks = $upload_driver == 'local' ? 'public' : $upload_driver;
        } else {
            $whereExists[] = ['driver', '=', $disks == 'public' ? 'local' : $disks];
        }

        if ($disks == 'aliyun') {
            self::getConfig($disks);
        } elseif (strpos($disks,'qiniu') !== false) {
            self::getConfig($disks);
        }
        $disksConfig = config('filesystem.disks')[$disks];
        $newTitle = ($title && (pathinfo($title)['basename'] != "*.*")) ? pathinfo($title)['basename'] : '';
        if ($file_path != ($content = str_replace(['fileContents://'], '', $file_path)) && $newTitle) {
            ini_set('memory_limit', '1024M');
            $md5 = md5($content);
            //  【一】fileContents请求
            $whereExists[] = ['md5', '=', $md5];
            if ($file_exists = self::getAttachmentExists($whereExists, $newTitle)) {
                return $file_exists;
            } else {
                $dir =  pathinfo($title)['dirname'] . date('/Ym/d');
                utilFile::check_owner(config('app.upload_path') . DIRECTORY_SEPARATOR . $dir); //(创建)验证设置文件夹所有者
                $newName = md5((string) microtime(true))  . (isset(pathinfo($title)['extension']) ? '.' . pathinfo($title)['extension'] : ''); //文件名
                $save_path = $dir . '/' . $newName;
                // 移动到框架应用根目录/uploads/ 目录下
                \yzh52521\filesystem\facade\Filesystem::disk($disks)->write($save_path, $content); //本地文件迁移(生成$dir路径 www775目录-644文件)
                $full_path = config('app.upload_path') . DIRECTORY_SEPARATOR . $save_path; //服务器完整路径
                if (!file_exists($full_path)) {
                    return false; //服务器文件不存在
                }
                $file = new File($full_path);
                $file_info = [
                    'uid'    => 0,
                    'name'   => $newTitle, //原文件名
                    'mime'   => $file->getMime(),
                    'path'   => $disksConfig['url'] . '/' . $save_path, //存储路径
                    'ext'    => array_keys(self::$ext_arr, $file->extension()) ? $file->extension() : utilFile::get_ext($file->getMime()),
                    'size'   => $file->getSize(),
                    'md5'    => $md5,
                    'sha1'   => $file->sha1(),
                    'driver' => $disks,
                    'module' => $module,
                ];
                $foreign_path ? $file_info['foreign_path'] = $foreign_path : ''; //指定外键路径
                // 写入数据库
                if ($file_add = self::create($file_info)) {
                    return $file_add;
                }
            }
        } else if (($file_path != str_replace(['https://' . config('app.app_host') . '/', 'http://' . config('app.app_host') . '/', public_path()], '', $file_path)) || (file_exists(public_path() . $file_path) && is_file(public_path() . $file_path))) {
            // 【二】file_path为本服务器文件(站点/完整)路径
            $file_path = str_replace(['https://' . config('app.app_host') . '/', 'http://' . config('app.app_host') . '/', public_path()], '', $file_path);
            $full_path = public_path() . $file_path; //服务器完整路径

            if (!file_exists($full_path)) {
                return false; //服务器文件不存在
            }
            $file = new File($full_path);
            if ($file->getSize() == 0) {
                return false; //服务器文件不存在
            }
            $whereExists[] = ['md5', '=', $file->md5()];
            if ($file_exists = self::getAttachmentExists($whereExists, $newTitle)) {
                if ($file_path != $file_exists['path']) {
                    if (file_exists($full_path)) {
                        // 删除临时图片
                        unlink($full_path);
                        $pathinfo = pathinfo($full_path);
                        if (count(scandir($pathinfo['dirname'])) <= 2) {
                            // 如果目录中只包含 . 和 ..（默认的当前目录和上级目录），则删除该目录
                            rmdir($pathinfo['dirname']);
                        }
                    }
                }
                return $file_exists;
            } else {
                if ($file->getSize() < 10000) {
                    //文件小于10k,验证文件内容
                    $content = file_get_contents($full_path);
                    if (strpos($content, '<p>The requested URL was not found on this server. Sorry for the inconvenience.<br/>') !== false || strpos($content, '<div class="head"><p class="et">ERROR</p><p class="ct">没有找到相关信息~</p></div>') !== false) {
                        return false; //服务器文件不存在
                    }
                }
                $pathinfo = pathinfo($file_path);
                $dir = ($title ? pathinfo($title)['dirname']  : str_replace('uploads/', '', $pathinfo['dirname'])) . date('/Ym/d', filectime($full_path));
                utilFile::check_owner(config('app.upload_path') . DIRECTORY_SEPARATOR . $dir); //(创建)验证设置文件夹所有者
                // 移动到框架应用根目录/uploads/ 目录下
                $save_path = \yzh52521\filesystem\facade\Filesystem::disk($disks)->putFile($dir, $file, function () use ($file) {
                    return md5((string) microtime(true)) . (array_keys(self::$ext_arr, $file->extension()) ? '' : '.' . utilFile::get_ext($file->getMime()));
                }); //本地文件迁移(生成$dir路径 www775目录-644文件)
                // 附件信息
                $file_info = [
                    'uid'    => session('user_auth.uid') ?? 0,
                    'name'   => $newTitle ? $newTitle : (array_keys(self::$ext_arr, $file->extension()) ? $pathinfo['basename'] : $pathinfo['basename'] . '.' . utilFile::get_ext($file->getMime())), //原文件名
                    'mime'   => $file->getMime(),
                    'path'   => $disksConfig['url'] . '/' . $save_path, //存储路径
                    'ext'    => (array_keys(self::$ext_arr, $file->extension()) ? $file->extension() : utilFile::get_ext($file->getMime())),
                    'size'   => $file->getSize(),
                    'md5'    => $file->md5(),
                    'sha1'   => $file->sha1(),
                    'driver' => $disks,
                    'module' => $module,
                ];
                $foreign_path ? $file_info['foreign_path'] = $foreign_path : ''; //指定外键路径
                // 写入数据库
                if ($file_add = self::create($file_info)) {
                    if ($file_path != $file_add['path']) {
                        // 删除临时图片
                        unlink($full_path);
                        $pathinfo = pathinfo($full_path);
                        if (count(scandir($pathinfo['dirname'])) <= 2) {
                            // 如果目录中只包含 . 和 ..（默认的当前目录和上级目录），则删除该目录
                            rmdir($pathinfo['dirname']);
                        }
                    }
                    return $file_add;
                }
            }
        } else if ($file_path != str_replace(['https://', 'http://'], '', $file_path)) {
            // 【三】file_path为远程url文件

            if (strpos($file_path, 'attachment.recer.zmail.pub') !== false && strpos($file_path, ' ') !== false) {
                //漏洞处理(忽略验证)
            } else {
            if (!filter_var($file_path, FILTER_VALIDATE_URL)) {
                return false; //非有效格式url
            }
            }
            $content = utilHttp::curl_response($file_path, [], $header); //下载文件
            if ($header['download_content_length'] <= 0) {
                return false; //远程文件不存在
            }
            $md5 = md5($content);
            $whereExists[] = ['md5', '=', $md5];
            if ($file_exists = self::getAttachmentExists($whereExists, $newTitle)) {
                return $file_exists;
            } else {
                $parts  = parse_url($header['url']);
                $pathinfo = pathinfo($parts['query'] ?? $parts['path']);

                $dir = ($title ? pathinfo($title)['dirname']  : $parts['path']) . date('/Ym/d');
                $new_path = config('app.upload_path') . DIRECTORY_SEPARATOR . $dir; //服务器完整路径
                utilFile::check_owner($new_path); //(创建)验证设置文件夹所有者

                $mime = str_replace(['charset=utf-8', 'charset=iso-8859-1', ' ', ';'], '', strtolower($header['content_type'])); //本地文件方法:mime_content_type($content)
                $ext = utilFile::get_ext($mime) ?: ($pathinfo['extension'] ?? '');
                $newName = md5((string) microtime(true))  . ($ext ? '.' . $ext : ''); //文件名

                $save_path = $dir . '/' . $newName;
                // 移动到框架应用根目录/uploads/ 目录下
                \yzh52521\filesystem\facade\Filesystem::disk($disks)->write($save_path, $content); //本地文件迁移(生成$dir路径 www775目录-644文件)

                // 附件信息
                $file_info = [
                    'uid'    => 0,
                    'name'   => $newTitle ? $newTitle : (strpos($pathinfo['basename'], '.') !== false ? substr($pathinfo['basename'], 0, 250) : ''), //原文件名
                    'mime'   => $mime,
                    'path'   => $disksConfig['url'] . '/' . $save_path, //存储路径
                    'ext'    => $ext,
                    'size'   => $header['size_download'],
                    'md5'    => $md5,
                    'sha1'   => sha1($content),
                    'driver' => $disks,
                    'module' => $module,
                ];
                $foreign_path ? $file_info['foreign_path'] = $foreign_path : ''; //指定外键路径
                // 写入数据库
                if ($file_add = self::create($file_info)) {
                    return $file_add;
                }
            }
        } else if (!$file_path && $file = request()->file('file')) {
            // 【四】 post-file请求
            $whereExists[] = ['md5', '=', $file->md5()];
            if ($file_exists = self::getAttachmentExists($whereExists, $newTitle)) {
                return $file_exists;
            } else {
                $dir =  pathinfo($title)['dirname'] . date('/Ym/d');
                utilFile::check_owner(config('app.upload_path') . DIRECTORY_SEPARATOR . $dir); //(创建)验证设置文件夹所有者
                // 移动到框架应用根目录/uploads/ 目录下
                $save_path = \yzh52521\filesystem\facade\Filesystem::disk($disks)->putFile($dir, $file, function () use ($file) {
                    return md5((string) microtime(true)) . (array_keys(self::$ext_arr, $file->extension()) ? '' : '.' . utilFile::get_ext($file->getMime()));
                }); //本地文件迁移(生成$dir路径 www775目录-644文件)

                $file_info = [
                    'uid'    => 0,
                    'name'   => $newTitle, //原文件名
                    'mime'   => $file->getMime(),
                    'path'   => $disksConfig['url'] . '/' . $save_path, //存储路径
                    'ext'    => (array_keys(self::$ext_arr, $file->extension()) ? $file->extension() :  utilFile::get_ext($file->getMime())),
                    'size'   => $file->getSize(),
                    'md5'    => $file->md5(),
                    'sha1'   => $file->sha1(),
                    'driver' => $disks,
                    'module' => $module,
                ];
                $foreign_path ? $file_info['foreign_path'] = $foreign_path : ''; //指定外键路径
                // 写入数据库
                if ($file_add = self::create($file_info)) {
                    return $file_add;
                }
            }
        }
        return false;
    }

    // driver字段的修改器
    public function setDriverAttr($value, $data)
    {
        $value == 'public' ? $value = 'local' : '';
        if (($data['id'] ?? '')) {
            //非新增
            $item = self::getById($data['id']);
            $item['name'] = str_replace('/', '%2F', $item['name']); //url不识别字符
            $title = 'uploads/'.$item['module'].'/' . $item['name'];
            $file_exists = $this->toAttachment($item['path'], $item['module'], $title, $value == 'local' ? 'public' : $value);
            if ($file_exists) {
                $this->where('id', $file_exists['id'])->delete();
                if ($item['driver'] == 'aliyun') {
                    //原模式为aliyun
                    $disksConfig = self::getConfig($item['driver']);
                    \yzh52521\filesystem\facade\Filesystem::disk($item['driver'])->delete(str_replace($disksConfig['url'] . '/', '', $item['path'])); //本地文件迁移(生成$dir路径 www775目录-644文件)
                } elseif (strpos($item['driver'],'qiniu') !== false) {
                    $disksConfig = self::getConfig($item['driver']);
                    \yzh52521\filesystem\facade\Filesystem::disk($item['driver'])->delete(str_replace($disksConfig['url'] . '/', '', $item['path'])); //本地文件迁移(生成$dir路径 www775目录-644文件)
                }
                $this->where('id', $data['id'])->update(['path' => $file_exists['path'], 'md5' => $file_exists['md5']]);
                if ($item['foreign_path'] && ($path = self::getUrl($file_exists['driver'] ?? '', $file_exists['path'] ?? ''))) {
                    $foreign_paths = explode(';', $item['foreign_path']); //外键路径解析
                    foreach ($foreign_paths as $key => $v) {
                        $foreign_path = explode("@", $v);
                        if ($foreign_path[1] ?? '') {
                            $id_url = explode(":", $foreign_path[0]);
                            if (($id_url[0] ?? '') && $data['id'] != $file_exists['id']) {
                                if ($detail_id = Db::name($foreign_path[1])->where($id_url[0], $file_exists['id'])->value('id')) {
                                    Db::name($foreign_path[1])->where('id', $detail_id)->update([$id_url[0] => $data['id']]);
                                }
                            }
                            unset($id_url[0]);
                            foreach ($id_url as $key => $url) {
                                if ($detail = Db::name($foreign_path[1])->whereLike($url, "%" .  str_replace(['http://', 'https://'], '', self::getUrl($item['driver'], $item['path'], false)) . "%")->find()) {
                                    Db::name($foreign_path[1])->where('id', $detail['id'])->update([$url => $path]);
                                }
                            }
                        }
                    }
                }
            } else {
                //上传附件不成功
                self::where('id', $data['id'])->update(['status' => 0]); //无效
            }
        }
        return $value;
    }

    public static function onBeforeDelete($value)
    {
        if ($value['driver'] == 'local') {
            $real_path       = realpath(config('app.upload_path') . '/../' . $value['path']);
            $real_path_thumb = realpath(config('app.upload_path') . '/../' . $value['thumb']);
            if (is_file($real_path) && !unlink($real_path)) {
                // return false;
            }
            if (is_file($real_path_thumb) && !unlink($real_path_thumb)) {
                // return false;
            }
        } elseif ($value['driver'] == 'aliyun') {
            //原模式为aliyun
            $disksConfig = self::getConfig($value['driver']);
            \yzh52521\filesystem\facade\Filesystem::disk($value['driver'])->delete(str_replace($disksConfig['url'] . '/', '', $value['path'])); //本地文件迁移(生成$dir路径 www775目录-644文件)
        } elseif (strpos($value['driver'],'qiniu') !== false) {
            //原模式为qiniu
            $disksConfig = self::getConfig($value['driver']);
            \yzh52521\filesystem\facade\Filesystem::disk($value['driver'])->delete(str_replace($disksConfig['url'] . '/', '', $value['path'])); //删除
        }
    }

    static public function getUrl($driver, $path, $checkExist = true)
    {
        if (!$driver || !$path) {
            return ''; //不存在
        }

        if ($driver == 'local') {
            // 本地服务器文件
            $path = trim($path, '/');
            if ($checkExist) {
                $full_path = public_path() . $path; //服务器完整路径
                if (!file_exists($full_path)) {
                    return ''; //服务器文件不存在
                }
            }
            $file_url = 'https://' . config('app.app_host') . "/"  . $path;
        } else {
            $disksConfig = self::getConfig($driver);
            $domain = str_replace(['https://', 'http://'], '', $disksConfig['url']);
            $file_path = str_replace([$domain . '/', '//'], '/', str_replace(['https://', 'http://'], '', $path));
            $file_url = $disksConfig['url'] . $file_path; // 远程url文件
            if ($checkExist) {
                $exists = \yzh52521\filesystem\facade\Filesystem::disk($driver)->exists($file_path);
                if (!$exists) {
                    return ''; //存储服务器文件不存在
                }
            }
        }
        return $file_url;
    }

    static public function getUrlByid($attachment_id, $islocalFull = false)
    {
        $file_exists = $attachment_id?self::getByid($attachment_id):null;
        if (($file_exists['driver'] ?? '') == 'local' && !$islocalFull) {
            // 本地服务器文件(路径补全)
            $path = trim($file_exists['path'] ?? '', '/');
            return PUBLIC_PATH  . $path;
        }
        return $file_exists ? (self::getUrl($file_exists['driver'] ?? '', $file_exists['path'] ?? '', false)) : '';
    }   
}
