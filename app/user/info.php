<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2019 广东卓锐软件有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------

/**
 * 模块信息
 */
return [
  
  // 行为配置
  'action' =>[
    [
      'module' => 'user',
      'name' => 'user_add',
      'title' => '添加用户',
      'remark' => '添加用户',
      'rule' => '',
      'log' => '[user|get_nickname] 添加了用户：[record|get_nickname]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'user_edit',
      'title' => '编辑用户',
      'remark' => '编辑用户',
      'rule' => '',
      'log' => '[user|get_nickname] 编辑了用户：[details]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'user_delete',
      'title' => '删除用户',
      'remark' => '删除用户',
      'rule' => '',
      'log' => '[user|get_nickname] 删除了用户：[details]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'user_enable',
      'title' => '启用用户',
      'remark' => '启用用户',
      'rule' => '',
      'log' => '[user|get_nickname] 启用了用户：[details]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'user_disable',
      'title' => '禁用用户',
      'remark' => '禁用用户',
      'rule' => '',
      'log' => '[user|get_nickname] 禁用了用户：[details]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'user_access',
      'title' => '用户授权',
      'remark' => '用户授权',
      'rule' => '',
      'log' => '[user|get_nickname] 对用户：[record|get_nickname] 进行了授权操作。详情：[details]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'role_add',
      'title' => '添加角色',
      'remark' => '添加角色',
      'rule' => '',
      'log' => '[user|get_nickname] 添加了角色：[details]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'role_edit',
      'title' => '编辑角色',
      'remark' => '编辑角色',
      'rule' => '',
      'log' => '[user|get_nickname] 编辑了角色：[details]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'role_delete',
      'title' => '删除角色',
      'remark' => '删除角色',
      'rule' => '',
      'log' => '[user|get_nickname] 删除了角色：[details]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'role_enable',
      'title' => '启用角色',
      'remark' => '启用角色',
      'rule' => '',
      'log' => '[user|get_nickname] 启用了角色：[details]',
      'status' => 1,
    ],
    [
      'module' => 'user',
      'name' => 'role_disable',
      'title' => '禁用角色',
      'remark' => '禁用角色',
      'rule' => '',
      'log' => '[user|get_nickname] 禁用了角色：[details]',
      'status' => 1,
    ],
  ],
  'database_prefix' => 'dp_',
];
