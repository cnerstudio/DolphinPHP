# thinkphp6升级到thinkphp8

## 官方文档:

```language
https://doc.thinkphp.cn/v8_0/setup.html
```

## 查看当前项止的thinkphp版本

```language
php think version
v6.1.0
```

## 1,升级原项目到可用的最新版本:

```language
composer update topthink/framework
...
php think version
v6.1.4
```

## 2,修改composer.json

```language
"require": {
        "php": ">=8.0.0",
        "topthink/framework": "^8.0",
        "topthink/think-orm": "^3.0",
        "topthink/think-filesystem": "^2.0"
    },
    "require-dev": {
        "symfony/var-dumper": ">=4.2",
    },
```

## 3,删除composer.lock

```language
rm composer.lock
```

## 4,升级前查看当前各库的版本:

```language
composer show
firebase/php-jwt          v6.3.1  A simple library to encode and decode JSON Web Tokens (JWT) in PHP. Should conform to the current spec.
psr/container             1.1.2   Common Container Interface (PHP FIG PSR-11)
psr/http-message          1.0.1   Common interface for HTTP messages
psr/log                   1.1.4   Common interface for logging libraries
psr/simple-cache          1.0.1   Common interfaces for simple caching
symfony/polyfill-mbstring v1.27.0 Symfony polyfill for the Mbstring extension
symfony/polyfill-php72    v1.27.0 Symfony polyfill backporting some PHP 7.2+ features to lower PHP versions
symfony/polyfill-php80    v1.27.0 Symfony polyfill backporting some PHP 8.0+ features to lower PHP versions
symfony/var-dumper        v4.4.47 Provides mechanisms for walking through any arbitrary PHP variable
topthink/framework        v6.1.4  The ThinkPHP Framework.
topthink/think-helper     v3.1.6  The ThinkPHP6 Helper Package
topthink/think-orm        v2.0.56 think orm
topthink/think-trace      v1.5    thinkphp debug trace
```


## 5,升级:

```language
composer install
No composer.lock file present. Updating dependencies to latest instead of installing from lock file. See https://getcomposer.org/install for more information.
Loading composer repositories with package information
Updating dependencies
Lock file operations: 14 installs, 0 updates, 0 removals
  - Locking firebase/php-jwt (v6.8.0)
  - Locking league/flysystem (2.5.0)
  - Locking league/mime-type-detection (1.11.0)
  - Locking psr/container (2.0.2)
  - Locking psr/http-message (1.1)
  - Locking psr/log (3.0.0)
  - Locking psr/simple-cache (3.0.0)
  - Locking symfony/polyfill-mbstring (v1.27.0)
  - Locking symfony/var-dumper (v6.3.0)
  - Locking topthink/framework (v8.0.1)
  - Locking topthink/think-filesystem (v2.0.2)
  - Locking topthink/think-helper (v3.1.6)
  - Locking topthink/think-orm (v3.0.11)
  - Locking topthink/think-trace (v1.6)
Writing lock file
Installing dependencies from lock file (including require-dev)
Package operations: 3 installs, 6 updates, 2 removals
  - Downloading league/mime-type-detection (1.11.0)
  - Downloading psr/container (2.0.2)
  - Downloading symfony/var-dumper (v6.3.0)
  - Downloading psr/simple-cache (3.0.0)
  - Downloading psr/log (3.0.0)
  - Downloading topthink/think-orm (v3.0.11)
  - Downloading topthink/framework (v8.0.1)
  - Downloading league/flysystem (2.5.0)
  - Downloading topthink/think-filesystem (v2.0.2)
  - Removing symfony/polyfill-php80 (v1.27.0)
  - Removing symfony/polyfill-php72 (v1.27.0)
  - Installing league/mime-type-detection (1.11.0): Extracting archive
  - Upgrading psr/container (1.1.2 => 2.0.2): Extracting archive
  - Upgrading symfony/var-dumper (v4.4.47 => v6.3.0): Extracting archive
  - Upgrading psr/simple-cache (1.0.1 => 3.0.0): Extracting archive
  - Upgrading psr/log (1.1.4 => 3.0.0): Extracting archive
  - Upgrading topthink/think-orm (v2.0.61 => v3.0.11): Extracting archive
  - Upgrading topthink/framework (v6.1.4 => v8.0.1): Extracting archive
  - Installing league/flysystem (2.5.0): Extracting archive
  - Installing topthink/think-filesystem (v2.0.2): Extracting archive
2 package suggestions were added by new dependencies, use `composer suggest` to see details.
Generating autoload files
> @php think service:discover
Succeed!
> @php think vendor:publish
File /data/php/tpapibase/config/trace.php exist!
Succeed!
4 packages you are using are looking for funding.
Use the `composer fund` command to find out more!
```

## 查看升级后的版本:

```language
composer show
firebase/php-jwt           v6.8.0  A simple library to encode and decode JSON Web Tokens (JWT) in PHP. Should conform to the current spec.
league/flysystem           2.5.0   File storage abstraction for PHP
league/mime-type-detection 1.11.0  Mime-type detection for Flysystem
psr/container              2.0.2   Common Container Interface (PHP FIG PSR-11)
psr/http-message           1.1     Common interface for HTTP messages
psr/log                    3.0.0   Common interface for logging libraries
psr/simple-cache           3.0.0   Common interfaces for simple caching
symfony/polyfill-mbstring  v1.27.0 Symfony polyfill for the Mbstring extension
symfony/var-dumper         v6.3.0  Provides mechanisms for walking through any arbitrary PHP variable
topthink/framework         v8.0.1  The ThinkPHP Framework.
topthink/think-filesystem  v2.0.2  The ThinkPHP6.1 Filesystem Package
topthink/think-helper      v3.1.6  The ThinkPHP6 Helper Package
topthink/think-orm         v3.0.11 the PHP Database&ORM Framework
topthink/think-trace       v1.6    thinkphp debug trace
```