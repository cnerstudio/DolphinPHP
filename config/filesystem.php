<?php

use think\facade\Env;

return [
    'default' => Env::get('filesystem.driver', 'local'),
    'disks'   => [
        'local'  => [
            'type' => 'local',
            'root' => app()->getRuntimePath() . 'storage',
        ],
        'public' => [
            'type'       => 'local',
            'root'       => app()->getRootPath() . 'public/uploads',
            'url'        => '/uploads',
            'visibility' => 'public',
        ],
        'aliyun' => [
            'type'          => 'aliyun',
            'accessId'      => '',
            'accessSecret'  => '',
            'bucket'     => '',
            'endpoint'   => '',
            'url'        => '',
            'visibility' => 'public',
        ],
        'qiniu' => [//七牛云-华北(默认)
            'type'          => 'qiniu',
            'accessKey'      => '',
            'secretKey'  => '',
            'bucket'     => '',
            'domain'     => '',
            'url'        => '',//CDN-HTTP   第 0 GB 至 10 GB (含)   免费    https://portal.qiniu.com/financial/price?product=fusion
        ],
        'qiniu-hd' => [//七牛云-华东
            'type'          => 'qiniu',
            'accessKey'      => '',
            'secretKey'  => '',
            'bucket'     => '',
            'domain'     => '',
            'url'        => '',
        ],
        // 更多的磁盘配置信息
    ]
];
