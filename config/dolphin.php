<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2019 广东卓锐软件有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------

return [
    // 产品信息
    'product_name'      => 'DolphinPHP',
    'product_version'   => '3.0.2',
    'build_version'     => '3.0.2.20240729',
    'product_website'   => 'http://www.dolphinphp.com',
    'develop_team'      => 'cnerstudio',

    // 公司信息
    'company_name'    => '信儿工作室',
    'company_website' => 'http://www.cnerstudio.com',
];